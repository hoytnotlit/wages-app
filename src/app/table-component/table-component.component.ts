import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.css']
})
export class TableComponentComponent implements OnInit {

  @Input() data: Array<any>;
  startDate: Date;
  endDate: Date;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.data != undefined) {
      this.getInitialDates();
      //sort by id
      this.data = this.data.sort((a, b) => {
        return a.id > b.id ? 1 : -1;
      });
    }
  }

  //display total wages for person
  getTotalWages(person) {
    return person.shifts.map(s => s.wages).reduce((a, b) => a + b, 0);
  }

  //display end and start date of data
  getInitialDates() {
    let min;
    let max;
    for (let person of this.data) {
      let dates = person.shifts.map(s => s.start).sort((a, b) => {
        return Date.parse(a) > Date.parse(b);
      });
      if (min == undefined || dates[0].getTime() < min.getTime())
        min = dates[0];

      if (max == undefined || dates[dates.length - 1].getTime() < max.getTime())
        max = dates[dates.length - 1];
    }
    this.startDate = min;
    this.endDate = max;
  }

}
