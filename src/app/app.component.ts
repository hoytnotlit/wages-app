import { Component, ViewChild } from '@angular/core';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);
const hourlypay = 4.25;
const eveningpay = 1.25;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild("csvReader", { static: false }) csvReader: any;
  wagesData: Array<Employee>;

  uploadListener(e) {
    let input = e.target;
    let reader = new FileReader();
    reader.readAsText(input.files[0]);

    reader.onload = () => {
      let data = reader.result;
      let dataArray = (<string>data).split(/\r\n|\n/);
      this.wagesData = this.parseData(dataArray);
    };
  }

  //parse csv data
  parseData(dataArray) {
    let result = [];

    //exclude first row (header) 
    for (let i = 1; i < dataArray.length; i++) {
      let row = (<string>dataArray[i]).split(',');
      //exclude possible empty rows
      if (row.length == 5) {
        let existing = result.find(r => r.id == row[1]);
        //add new person 
        if (existing == undefined) {
          let parsedRow = new Employee();
          parsedRow.name = row[0];
          parsedRow.id = row[1];
          parsedRow = this.addShift(parsedRow, row);
          result.push(parsedRow);
        } else {
          //add shift + wages for existing person
          existing = this.addShift(existing, row);
        }
      }
    }
    return result;
  }

  //add shift for employee
  addShift(person, row) {
    //set end and start dates
    let start = this.parseDate(row[2], row[3]);
    let end = this.parseDate(row[2], row[4]);
    // if ending on next day (eg. 22-06) increment date
    if (end.getTime() < start.getTime()) {
      end.setDate(end.getDate() + 1);
    }
    //calc wages
    let wages = this.calcWages(start, end);
    person.shifts.push({ start: start, end: end, wages: wages });
    return person;
  }

  //get wages for shift
  calcWages(start, end) {
    //get difference in milliseconds and convert to hours 
    let hours = (end.getTime() - start.getTime()) / 36e5;
    //regular daily wage
    let dailyWage = hours * hourlypay;

    //evening compensation (for h's between 19-6)
    let eveningWage = 0;
    let eveningHours = 0;
    //evening start date 
    let datestr = "";
    datestr = datestr.concat(start.getFullYear(), '-', start.getMonth(), '-', start.getDate(), ' 19:00');
    let eveningStart = moment(datestr, 'YYYY-MM-DD HH:mm');
    //evening end date (ending next day)
    datestr = "";
    datestr = datestr.concat(start.getFullYear(), '-', start.getMonth(), '-', start.getDate() + 1, ' 6:00');
    let eveningEndTomorrow = moment(datestr, 'YYYY-MM-DD HH:mm');

    //use moment to check if ranges overlap
    const shiftRange = moment.range(this.parseDateToMomentDate(start), this.parseDateToMomentDate(end));
    const eveningRange = moment.range(eveningStart, eveningEndTomorrow);

    if (shiftRange.overlaps(eveningRange)) {
      //get amount of evening hours 
      let intersect = shiftRange.intersect(eveningRange);
      eveningHours = (intersect.end.valueOf() - intersect.start.valueOf()) / 36e5;
    }
    //check for early morning shift 
    let eveningEnd = new Date(start.getFullYear(), start.getMonth(), start.getDate());
    eveningEnd.setHours(6, 0, 0);
    //get amount of evening hours if start time is before endTime on same day (eg. 5-10am)
    if (start.getTime() < eveningEnd.getTime()) {
      eveningHours += (eveningEnd.getTime() - start.getTime()) / 36e5;
    }

    if (eveningHours > 0) {
      eveningWage = eveningHours * eveningpay;
    }

    //overtime compensation
    let overtimeWage = 0;
    let overtimeHours = hours - 8;
    while (overtimeHours > 0) {
      if (overtimeHours <= 3) {
        // First 3 Hours > 8 Hours ​ = Hourly Wage + 25%
        overtimeWage += overtimeHours * hourlypay * 0.25;
        overtimeHours = 0;
      }
      else if (overtimeHours == 4) {
        // Next 1 Hour after first 3 = Hourly Wage + 50%
        overtimeWage += hourlypay * 0.5;
        overtimeHours -= 1;
      }
      else if (overtimeHours > 0) {
        // After first 4 hours = Hourly Wage + 100%
        let exceedingHours = overtimeHours - 4; //amount of hours over the 4 hours with different pay
        overtimeWage += exceedingHours * hourlypay;
        overtimeHours -= exceedingHours;
      }
    }

    //Total Daily Pay = Regular Daily Wage + Evening Work Compensation + Overtime Compensations
    let total = dailyWage + eveningWage + overtimeWage;
    return total;
  }

  //parse date object from csv data
  parseDate(date, time) {
    let dateSplit = date.split(".");
    let timeSplit = time.split(":");
    let dateResult = new Date(parseInt(dateSplit[2]), parseInt(dateSplit[1]), parseInt(dateSplit[0]));
    dateResult.setHours(parseInt(timeSplit[0]), parseInt(timeSplit[1]), 0);
    return dateResult;
  }

  //parse date object to moment date
  parseDateToMomentDate(date) {
    let datestr = "";
    datestr = datestr.concat(date.getFullYear(), '-', date.getMonth(), '-', date.getDate(), ' ', date.getHours(), ':', date.getMinutes());
    return moment(datestr, 'YYYY-MM-DD HH:mm');
  }

}

export class Employee {
  public id: string;
  public name: string;
  public shifts: Array<Shift> = new Array<Shift>();
}

export class Shift {
  public start: Date;
  public end: Date;
  public wages: number;
}

